import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bookshelves';
  constructor(){
    var config = {
      apiKey: "AIzaSyAvKBPLFlPaGFFtBGotble8FZ_OF-wLYHo",
      authDomain: "bookshelves-d108e.firebaseapp.com",
      databaseURL: "https://bookshelves-d108e.firebaseio.com",
      projectId: "bookshelves-d108e",
      storageBucket: "bookshelves-d108e.appspot.com",
      messagingSenderId: "536977238068"
    };
    firebase.initializeApp(config);
  }
}
